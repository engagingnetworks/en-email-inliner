# Email Inliner

# Google Doc
https://docs.google.com/document/d/1e28nsFamS7VDmkojuHqSbAMUjx4QRdSGni9EFQVq14o/edit

## Windows Deployment
Run Terminal, go to the email-inliner root directory, then run deploy.bat. Make sure deploy.bat has the correct VERSION. Requires an AWS profile named EN.
```
cd email-inliner
git pull
deploy.bat
```
Review push status in https://us-east-2.console.aws.amazon.com/ecr/repositories/private/595139649883/email-inliner.

## Production Release
After a Windows deployment, tag the latest image with the release tag.
```
release.bat
```

## Linux Prerequisites
yum install docker; service docker start; chkconfig docker on

## Linux Deployment
The scripts are stored in the enapps S3 bucket under scripts/email-inliner.
```
cd ~/email-inliner
./aws-ecr-login.sh
./start.sh
```

## Linux Uninstall
```
./stop.sh
```

## Git
https://bitbucket.org/engagingnetworks/en-email-inliner

git@bitbucket.org:engagingnetworks/en-email-inliner.git

## Dockerfile FROM ruby
https://hub.docker.com/_/ruby/

## API
The REST API is modeled from http://premailer.dialect.ca/api, but supports only a small subset. For the request, you can only pass "url" or "html". For the response, it only returns "documents.html". The output html expires in 10 minutes. Here�s a sample request and response, using the Dev server.

Request:
```
POST http://127.0.0.1:4567/api/0.1/documents?url=http://dialect.ca/premailer-tests/base.html 
```

curl:
```
curl -X POST http://127.0.0.1:4567/api/0.1/documents?html="<html>...</html>"
```

Response:
```
{
  "documents": { 
    "html": "http://127.0.0.1:4567/html/15218b1e-fd16-4f87-9c4a-957e7b7ad182.html" 
  }
} 
```
