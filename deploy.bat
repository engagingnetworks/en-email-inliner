@echo off
set VERSION=2.0.0

docker build --no-cache -t en/email-inliner .

aws ecr get-login-password --profile EN | docker login --username AWS --password-stdin 595139649883.dkr.ecr.us-east-2.amazonaws.com

docker tag en/email-inliner 595139649883.dkr.ecr.us-east-2.amazonaws.com/email-inliner:latest
docker tag en/email-inliner 595139649883.dkr.ecr.us-east-2.amazonaws.com/email-inliner:%VERSION%

docker push 595139649883.dkr.ecr.us-east-2.amazonaws.com/email-inliner:latest
docker push 595139649883.dkr.ecr.us-east-2.amazonaws.com/email-inliner:%VERSION%
